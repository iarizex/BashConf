source ~/.alias
source ~/.set_prompt
source ~/bin/functions.bash

# Exporting ~/bin path
PATH=/Users/iarizex/bin:$PATH

# Exporting local lib path
LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=/opt/local/lib:$LD_LIBRARY_PATH

# Try to solve the problems with Matplotlib
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Update path for gettext
export PATH="/usr/local/opt/gettext/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/gettext/lib"
export CPPFLAGS="-I/usr/local/opt/gettext/include"

# Exporting ~/bini/dart-sass path
PATH=/Users/iarizex/bin/dart-sass:$PATH

# Java SDK configuration
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-14.jdk/Contents/Home
export PATH=$JAVA_HOME/bin:$PATH

# TexLive 2020 configuration
export PATH="/usr/local/texlive/2020/bin/x86_64-darwin:$PATH"

# SurveyMonkey access token
export SURVEYMONKEY_ACCESS_TOKEN=j6sDjrzcuc4x0Rn.TW36d8X8n2MISWteWSqO9bN95ncrgt3qvqwFI37WTBXk.4KiN0eBpH9QKFqMy8CZYWxjP6Aci6sGzMcsVf3leRlxbf7LqGN662nxT5qYhvKnm3B8

# Load Angular CLI autocompletion.
source <(ng completion script)

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/iarizex/opt/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/iarizex/opt/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/Users/iarizex/opt/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

